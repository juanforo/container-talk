#README

Los ejemplos que se muestran en este repo fueron sacados de la [documentación de Rkt](https://github.com/rkt/rkt/blob/master/Documentation/getting-started-guide.md) y la [guía de lxdock](https://github.com/lxdock/lxdock).

## ¿Cómo correr los ejemplos?

**Requisitos**

* _Vagrant 1.8.5+_

Dentro de cada una de las carpetas correr el Vagrantfile.

    vagrant up

Para borrar el Workspace

    vagrant destroy
