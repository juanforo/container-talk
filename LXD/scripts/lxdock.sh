#!/bin/bash
set -x -e
sudo apt-get install golang ca-certificates git -y
runuser -l ubuntu -c 'mkdir $HOME/go'
runuser -l ubuntu -c 'echo "export GOPATH=$HOME/go" >> ~/.bash_profile'
runuser -l ubuntu -c 'go get github.com/juanforo/go-example'
runuser -l ubuntu -c 'pushd $GOPATH/bin && ./go-example'
