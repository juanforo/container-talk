#!/bin/bash
set -x
pushd /vagrant/lxdock
export PATH=/home/vagrant/bin:/home/vagrant/.local/bin:/home/vagrant/venv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
lxdock -v up
wait
#$CONTAINER_NAME=lxc list -c n --format csv | cut -d'|' -f2`
lxc file push /vagrant/main.go myproject-example-113/home/ubuntu/main.go
