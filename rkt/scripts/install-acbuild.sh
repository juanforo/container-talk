#!/bin/sh
set -x
wget -q https://github.com/containers/build/releases/download/v0.4.0/acbuild-v0.4.0.tar.gz
tar -xf acbuild-v0.4.0.tar.gz
cd /home/vagrant/acbuild-v0.4.0
mv acbuild* /usr/bin
sudo apt-get install systemd-container
